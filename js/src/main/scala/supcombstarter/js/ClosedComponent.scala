package supcombstarter.js

import java.util.UUID

import cats.effect.IO
import monix.reactive.Observable
import outwatch.Handler
import outwatch.dom.BasicVNode
import outwatch.dom.dsl._

final case class ClosedComponent private (valueHandler: Handler[String]) {

  private val componentId: UUID = UUID.randomUUID

  val valueStream: Observable[String] = valueHandler

  def node: BasicVNode =
    div(
      cls := "container",
      marginTop := "50px",
      div(
        cls := "form-row align-items-center",
        div(
          cls := "col-auto",
          label(
            cls := "sr-only",
            `for` := s"$componentId-searchInputId",
            "Some Text",
          ),
          input(
            `type` := "text",
            cls := "form-control mb-2",
            id := s"$componentId-searchInputId",
            placeholder := "Some Text",
            onInput.value --> valueHandler,
            value <-- valueHandler,
          ),
        ),
      ),
    )

}

object ClosedComponent {

  def init: IO[ClosedComponent] =
    for {
      valueStream <- Handler.create[String]("initial")
    } yield ClosedComponent(valueStream)

}
