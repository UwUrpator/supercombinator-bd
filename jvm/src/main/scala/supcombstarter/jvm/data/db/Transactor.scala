package supcombstarter.jvm.data.db

import cats.effect._
import cats.~>
import slick.basic.DatabaseConfig
import slick.dbio.DBIO
import slick.jdbc.{JdbcBackend, JdbcProfile}

// from https://github.com/kubukoz/slick-effect/blob/master/transactor/src/main/scala/slickeffect/Transactor.scala
trait Transactor[F[_]] {

  def transact[A](dbio: DBIO[A]): F[A] = transactK(dbio)
  def transactK: DBIO ~> F

  def configure(f: DBIO ~> DBIO): Transactor[F] =
    Transactor.liftK(f andThen transactK)

}

object Transactor {
  def apply[F[_]](implicit F: Transactor[F]): Transactor[F] = F

  def fromDatabase[F[_]: Async: ContextShift](
    dbF: F[JdbcBackend#DatabaseDef],
  ): Resource[F, Transactor[F]] =
    Resource.make(dbF)(db => Async.fromFuture(Sync[F].delay(db.shutdown))).map {
      db =>
        liftK {
          new (DBIO ~> F) {
            def apply[A](dbio: DBIO[A]): F[A] =
              Async.fromFuture(Sync[F].delay(db.run(dbio)))
          }
        }
    }

  def fromDatabaseConfig[F[_]: Async: ContextShift](
    dbConfig: DatabaseConfig[_ <: JdbcProfile],
  ): Resource[F, Transactor[F]] =
    fromDatabase(Sync[F].delay(dbConfig.db))

  def liftK[F[_]](f: DBIO ~> F): Transactor[F] =
    new Transactor[F] {
      override val transactK: DBIO ~> F = f
    }
}
