package supcombstarter.jvm.data.db

import com.github.tminglei.slickpg.{PgCirceJsonSupport, _}
import com.github.tminglei.slickpg.agg.PgAggFuncSupport
import com.github.tminglei.slickpg.window.PgWindowFuncSupport
import slick.ast.BaseTypedType
import slick.jdbc.{GetResult, JdbcCapabilities}
import supcombstarter.jvm.models.lambda
import supcombstarter.jvm.models.lambda.Id

trait SlickPgProfile
    extends ExPostgresProfile
    with PgCirceJsonSupport
    with PgArraySupport
    with PgDate2Support
    with PgRangeSupport
    with PgHStoreSupport
    with PgSearchSupport
    with PgNetSupport
    with PgLTreeSupport
    with PgWindowFuncSupport
    with PgAggFuncSupport {

  def pgjson = "jsonb"

  // Add back `capabilities.insertOrUpdate` to enable native `upsert` support; for postgres 9.5+
  override protected def computeCapabilities =
    super.computeCapabilities + JdbcCapabilities.insertOrUpdate

  override val api = SlickPgApi

  object SlickPgApi
      extends API
      with JsonImplicits
      with CirceJsonPlainImplicits
      with ArrayImplicits
      with DateTimeImplicits
      with NetImplicits
      with LTreeImplicits
      with RangeImplicits
      with HStoreImplicits
      with SearchImplicits
      with SearchAssistants
      with Date2DateTimePlainImplicits
      with GeneralAggFunctions {

    implicit val strListTypeMapper: DriverJdbcType[List[String]] =
      new SimpleArrayJdbcType[String]("text").to(_.toList)

    implicit def idMapping[T, U: BaseColumnType]: BaseTypedType[Id[T, U]] =
      MappedColumnType.base[Id[T, U], U](
        _.value,
        lambda.Id.apply,
      )

    implicit def idGetResult[TRef, TVal](implicit
      getTValResult: GetResult[TVal],
    ): GetResult[Id[TRef, TVal]] =
      getTValResult.andThen(lambda.Id.apply)

  }

}

object SlickPgProfile extends SlickPgProfile
