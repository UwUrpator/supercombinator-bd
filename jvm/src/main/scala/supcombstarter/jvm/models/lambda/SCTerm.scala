package supcombstarter.jvm.models.lambda

import cats.effect.Sync
import io.circe.generic.JsonCodec
import org.http4s.circe._
import org.http4s.{EntityDecoder, EntityEncoder}
import supcombstarter.jvm.models.lambda

@JsonCodec sealed trait SCTermReference

@JsonCodec final case class SCVariable(name: String) extends SCTermReference {
  override def toString(): String = name
}

@JsonCodec final case class SCDefinition(
  variables: List[SCVariable],
  term: SCTermReference,
  name: String,
) extends SCTermReference {
  override def toString: String =
    "$ " + name + " = [" + variables.mkString(" ") + "] " + term
}

@JsonCodec final case class SCApplication(
  function: SCTermReference,
  argument: SCTermReference,
) extends SCTermReference {
  override def toString: String = s"($function $argument)"
}

@JsonCodec final case class SCTermData(
  text: String,
  exprType: String,
  reference: Option[SCTermReference],
)

@JsonCodec final case class SCTerm(
  id: SCTerm.Id,
  data: SCTermData,
)

object SCDefinition {
  def apply(vars: List[SCVariable], term: SCTermReference): SCDefinition =
    SCDefinition(vars, term, "sc" + term.hashCode().toString)
}

object SCTermReference {
  implicit def SCTermReferenceEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, SCTermReference] =
    jsonOf[F, SCTermReference]
}

object SCTermData {
  implicit def SCTermDataEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, SCTermData] =
    jsonOf[F, SCTermData]
}

object SCTerm {

  type Id = lambda.Id[SCTerm, Int]

  implicit def SCTermEntityEncoder[F[_]]: EntityEncoder[F, Option[SCTerm]] =
    jsonEncoderOf[F, Option[SCTerm]]

  implicit def SCTermEntityDecoder[F[_]: Sync]: EntityDecoder[F, SCTerm] =
    jsonOf[F, SCTerm]
}
