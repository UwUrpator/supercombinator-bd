package supcombstarter.jvm.models.inference

import cats.effect.Sync
import io.circe.generic.JsonCodec
import org.http4s.circe._
import org.http4s.EntityDecoder

@JsonCodec sealed trait Type

@JsonCodec case class Tyvar(a: String) extends Type {
  override def toString: String = a
}

@JsonCodec case class Arrow(t1: Type, t2: Type) extends Type {
  override def toString: String = "(" + t1 + "->" + t2 + ")"
}

object Tyvar {
  implicit def TyvarEntityDecoder[F[_]: Sync]: EntityDecoder[F, Tyvar] =
    jsonOf[F, Tyvar]
}

object SCTermData {
  implicit def ArrowEntityDecoder[F[_]: Sync]: EntityDecoder[F, Arrow] =
    jsonOf[F, Arrow]
}
