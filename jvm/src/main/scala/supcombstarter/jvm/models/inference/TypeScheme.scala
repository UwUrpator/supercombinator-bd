package supcombstarter.jvm.models.inference

import supcombstarter.jvm.core.inference.TypeInferencer

case class TypeScheme(tyvars: List[Tyvar], tpe: Type) {
  def newInstance(): Type =
    tyvars.foldLeft(Subst.empty())((s, tv) =>
      s.extend(tv, TypeInferencer.newTyvar()),
    )(tpe)
}
