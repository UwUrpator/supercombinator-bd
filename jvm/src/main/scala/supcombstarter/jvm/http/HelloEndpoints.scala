package supcombstarter.jvm.http

import java.time.Instant

import cats.effect.{ContextShift, IO}
import supcombstarter.models.Hello
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import scala.concurrent.ExecutionContext

object HelloEndpoints extends Http4sDsl[IO] {

  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  object ValueMatcher extends QueryParamDecoderMatcher[String]("value")

  def endpoints(): HttpRoutes[IO] =
    HttpRoutes.of[IO] { case GET -> Root / "hello" :? ValueMatcher(value) =>
      Ok(Hello(Instant.now.getEpochSecond, s"Hi, $value").asJson)
    }

}
