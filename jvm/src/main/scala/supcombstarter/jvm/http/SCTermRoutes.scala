package supcombstarter.jvm.http

import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.effect.Sync
import supcombstarter.jvm.data.db.{SCTermDBIO, Transactor}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import mouse.string._
import supcombstarter.jvm.core.inference.{Predef, TypeInferencer}
import supcombstarter.jvm.core.lambda.SCCore
import supcombstarter.jvm.models.lambda.{
  Id,
  SCTerm,
  SCTermData,
  SCTermReference,
  Term,
}

object SCTermIdVar {
  def unapply(str: String): Option[SCTerm.Id] =
    str.parseIntOption.map(Id.apply[SCTerm, Int])
}

final class SCTermRoutes[F[_]: Sync](
  transactor: Transactor[F],
) extends Http4sDsl[F] {

  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "scterms" / SCTermIdVar(id) =>
        for {
          terms  <- transactor.transact(SCTermDBIO.byId(id))
          result <- Ok(terms)
        } yield result

      case request @ POST -> Root / "scterms" =>
        for {
          term <- request.as[SCTermReference]
          text = term.toString
          exprType = TypeInferencer.typeOf(Predef.env, term).toString
          id <- transactor.transact(
            SCTermDBIO.insert(SCTermData(text, exprType, Option(term))),
            //SCTermDBIO.insert(SCTermData(text, Option(term))),
          )
          result <- Ok(id)
        } yield result

      case request @ POST -> Root / "lift" =>
        for {
          term <- request.as[Term]
          scterm = SCCore.Lift(term)
          exprType = TypeInferencer.typeOf(Predef.env, scterm).toString
          id <- transactor.transact(
            SCTermDBIO.insert(
              SCTermData(scterm.toString, exprType, Option(scterm)),
              //SCTermData(scterm.toString, Option(scterm)),
            ),
          )
          result <- Ok(id)
        } yield result

      case request @ PUT -> Root / "scterms" =>
        for {
          term   <- request.as[SCTerm]
          _      <- transactor.transact(SCTermDBIO.update(term))
          result <- Ok()
        } yield result

      case DELETE -> Root / "scterms" / SCTermIdVar(id) =>
        for {
          _      <- transactor.transact(SCTermDBIO.delete(id))
          result <- Ok()
        } yield result
    }

}
