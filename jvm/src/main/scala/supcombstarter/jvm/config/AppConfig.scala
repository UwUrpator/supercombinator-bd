package supcombstarter.jvm.config

import pureconfig.generic.semiauto._

case class Http(port: Int, host: String)

object Http {
  implicit val httpConfigReader = deriveReader[Http]
}

case class AppConfig(http: Http, assets: String)

object AppConfig {
  implicit val appConfigReader = deriveReader[AppConfig]
}
