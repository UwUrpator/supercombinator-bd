CREATE TABLE supcomb (
    id serial NOT NULL PRIMARY KEY,
    text varchar NOT NULL,
    type varchar NOT NULL,
    reference jsonb NOT NULL
)

